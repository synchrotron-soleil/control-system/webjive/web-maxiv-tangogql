import asyncio
import sys
from unittest.mock import MagicMock

# before importing code under test we need to import the mock database
# proxy we are going to use for stubbing
from tangogql.feature_flags import FeatureFlags
from tests.unit.test_schema_mocks import MockDB, MockDeviceProxyCache

mock_base = MagicMock()
sys.modules["tangogql.schema.base"] = mock_base
mock_base.db = MockDB()
mock_base.proxies = MockDeviceProxyCache()
mock_base.toggle_flags = FeatureFlags()


class ResultTracker:
    """
    Basic utility class to track the events received
    for  for the subscribed attributes
    """

    def __init__(self, track_list=None):
        """initialises the list of results tested after execution"""
        if track_list is None:
            track_list = {}
        self.result_count = {i: 0 for i in track_list}

    def result_received_for(self, name):
        """
        return true if the results contain at least one result for 'name'

        :param name:
        """
        result = name in self.result_count
        assert result
        self.result_count[name] = self.result_count[name] + 1

    def the_number_of_results_received_for_each_was_exactly(self, count):
        """
        return true if exactly 'count' items were received over the execution

        :param count:
        :return:
        """
        return all(value == count for value in self.result_count.values())


def async_test(coro):
    """
    Run test asynchronously

    the code modules we are testing are coroutines
    so for each one we are going to
    run it asynchronously the way it will work when used for real
    so we will create an event loop and then run the code using it
    """

    def wrapper(*args, **kwargs):
        """Test wrapper - execute the test on  a new event lope"""
        loop = asyncio.new_event_loop()
        loop.run_until_complete(coro(*args, **kwargs))
        loop.close()

    return wrapper
